********************
Pythia Server
********************

A Pythia server runs the verifiable, oblivious Pythia protocol that protects passwords. This proof-of-concept server runs on Django, processes requests via HTTPS, and responds in JSON format. This implementation uses MongoDB as a datastore and uses the pythia-charm cryptographic module for Python.

=====
Prerequisites
=====

This server requires the Charm and PBC libraries. See this page for download
and installation instruction for Charm: <http://pages.cs.wisc.edu/~ace/install-charm.html>.

=====
Installation on Ubuntu 14.04
=====

Install required MongoDB and Django

.. code-block:: bash

    $ sudo apt-get -y install mongodb
    $ sudo pip install django

Start MongoDB

.. code-block:: bash

    $ sudo service mongod start

Start the Pythia server

.. code-block:: bash

    $ cd pythia-server
    $ python manage.py runserver

Test the Pythia server by visiting these URLs on the machine hosting the Pythia server:

* <http://127.0.0.1:8000/pythia/query-ecc>
* <http://127.0.0.1:8000/pythia/query-ecc?m=123&t=abc&w=098>