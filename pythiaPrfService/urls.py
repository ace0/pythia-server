"""
URLs for interacting with the Pythia PRF service.
"""

from django.conf.urls import include, url
import views

urlpatterns = [
	# Unique URL for each group type
	url(r'^query-ecc$', views.queryEcc, name='pythia-query-ecc'),
	url(r'^query-zq$', views.queryZq, name='pythia-query-zq'),
	url(r'^query-bls$', views.queryBls, name='pythia-query-bls'),
    
    # Url for OPRF queries
    url(r'^oquery-ecc$', views.oqueryEcc, name='pythia-oquery-ecc'),
    url(r'^oquery-bls$', views.oqueryBls, name='pythia-oquery-bls'),

	# Client key rotation
	url(r'^update-request$', views.updateRequest),
	url(r'^update-begin$', views.updateBegin),
	url(r'^update-complete$', views.updateComplete),
]
